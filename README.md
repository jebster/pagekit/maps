# Maps
A simple Pagekit extension to show a Google map at your desired position.


## Contributing
All pull requests are welcome. <br>
If you want to help translate, we use Transifex and you can help us translate here: https://www.transifex.com/jebster/jebster-pagekit/

